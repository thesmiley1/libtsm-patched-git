# libtsm-patched-git

AUR package for `libtsm-patched-git`

## Upstream

This is a soft fork of the [libtsm-patched-git AUR package][upstream].

[upstream]: https://aur.archlinux.org/packages/libtsm-patched-git/
